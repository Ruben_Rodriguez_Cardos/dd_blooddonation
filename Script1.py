import pandas as pd
import numpy as np
from sklearn import preprocessing
from sklearn.decomposition import PCA

import matplotlib.pyplot as plt

from sklearn.ensemble import RandomForestRegressor
from sklearn.covariance import EllipticEnvelope
from sklearn.svm import OneClassSVM

def loadData():
    df_train = pd.read_csv("Data/Train_data.csv")
    df_train = df_train.rename(columns={'Unnamed: 0': 'Id'})
    
    #Objetivo
    df_train_labels = df_train["Made Donation in March 2007"]
    
    #Columnas no neceserias
    #Id, solo identifica, el resultado va indexado en paralelo, por lo que el id no es necesario
    #Made Donation in March 2007 Objetivo
    df_train_values = df_train.drop(columns=['Id','Made Donation in March 2007'])

    df_test_values = pd.read_csv("Data/Test_data.csv")
    df_test_values = df_test_values.rename(columns={'Unnamed: 0': 'Id'})
    df_test_values = df_test_values.drop(columns=['Id'])

    return df_train_values, df_train_labels, df_test_values

def checkOutliners(X_pca):
    #Metodo 1
    clf = EllipticEnvelope()
    y_pred = clf.fit(X_pca).predict(X_pca) #Los -1 en y_pred son considerados outliners

    x = []
    y = []

    for i in X_pca:
        x.append(i[0])
        y.append(i[1])

    plt.scatter(x, y, c = y_pred ,alpha=0.5)
    plt.title("Outliers - Metodo 1")
    plt.show()

    #Metodo 2
    clf = OneClassSVM(nu=0.261, gamma=0.05)
    y_pred = clf.fit(X_pca).predict(X_pca) #Los -1 en y_pred son considerados outliners

    plt.title("Outliers - Metodo 2")
    plt.scatter(x, y, c = y_pred ,alpha=0.5)
    plt.show()

def plotInitialData(X_pca, labels):
    #Donante
    x_d = []
    y_d = []

    #No Donante
    x_nd = []
    y_nd = []

    for i,k in zip(X_pca, labels.values):
        if k == 0: #No donante
            x_d.append(i[0])
            y_d.append(i[1])
        else:
            x_nd.append(i[0])
            y_nd.append(i[1])

    plt.title("Donantes y No Donantes")
    plt.scatter(x_d, y_d ,alpha=0.5, label='Donante')
    plt.scatter(x_nd, y_nd ,alpha=0.5, label=' No Donante')
    plt.legend()
    plt.show()

def doPCA(data,n):
    #Normalización
    min_max_scaler = preprocessing.MinMaxScaler()
    states = min_max_scaler.fit_transform(data)

	#PCA Se explica el 92% de la varización de los datos
    estimator = PCA (n_components = n)
    X_pca = estimator.fit_transform(states)
    print(estimator.explained_variance_ratio_, "Varianza de los datos explicada: ",np.sum(estimator.explained_variance_ratio_), "%")
    return X_pca

def initialStudy(data, labels):
    
    X_pca = doPCA(data,2)

    #Visualización inicial de los datos
    plotInitialData(X_pca, labels)

    #Supuestamente existe una correlacion entre el volumen y el numero de veces que se ha donado (Todas las donaciones son de 250cc)
    print("Correlación entre Nº de donaciones y volumen donado: ", data['Number of Donations'].corr(data['Total Volume Donated (c.c.)']))
    

    #Se añade el ratio entre nº de donaciones y meses desde la primera donacion, obtieniendo asi un ratio de donacion/mes
    ratio_number_first = []
    number = data["Number of Donations"].values
    first = data["Months since First Donation"].values

    for i,j in zip(number,first):
        ratio_number_first.append(i/j)

    data["ratio_number_first"] = ratio_number_first
    data = data.drop(columns=['Total Volume Donated (c.c.)'])

    #Veo los oultliers
    checkOutliners(X_pca)

    #Correlaciones
    df_corr = data.corr()
    plt.matshow(df_corr)
    plt.xticks(range(len(df_corr.columns)), df_corr.columns)
    plt.yticks(range(len(df_corr.columns)), df_corr.columns)
    plt.colorbar()
    plt.show()



def doRandomForest(df_train_values, df_train_labels, df_test_values):
    
    ratio_number_first = []
    number = df_train_values["Number of Donations"].values
    first = df_train_values["Months since First Donation"].values

    for i,j in zip(number,first):
        ratio_number_first.append(i/j)

    df_train_values["ratio_number_first"] = ratio_number_first
    df_train_values = df_train_values.drop(columns=['Total Volume Donated (c.c.)'])
    
    ratio_number_first = []
    number = df_test_values["Number of Donations"].values
    first = df_test_values["Months since First Donation"].values

    for i,j in zip(number,first):
        ratio_number_first.append(i/j)

    df_test_values["ratio_number_first"] = ratio_number_first
    df_test_values = df_test_values.drop(columns=['Total Volume Donated (c.c.)'])

    #Normalización
    min_max_scaler = preprocessing.MinMaxScaler()
    states = min_max_scaler.fit_transform(df_train_values)

    min_max_scaler = preprocessing.MinMaxScaler()
    states_t = min_max_scaler.fit_transform(df_test_values)

    clf = RandomForestRegressor(n_estimators=1000, random_state=0)
    clf.fit(states, df_train_labels)
    predictions = clf.predict(states_t)

    rf=pd.DataFrame({'Attributes': df_test_values.columns , 'Random Forests':clf.feature_importances_})
    rf=rf.sort_values(by='Random Forests', ascending=0)

    print(rf)

    return predictions

def doSubmitFile(predictions):
    df = pd.read_csv("Data/BloodDonationSubmissionFormat.csv")
    df["Made Donation in March 2007"] = predictions
    df.to_csv("Data/Ready_BloodDonationSubmissionFormat.csv",index=False)

def main():
    df_train_values, df_train_labels, df_test_values = loadData()

    initialStudy(df_train_values, df_train_labels)

    predictions = doRandomForest(df_train_values, df_train_labels, df_test_values)
    doSubmitFile(predictions)
    
    

if __name__ == "__main__":
    main()