import pandas as pd
import numpy as np
from sklearn import preprocessing
from sklearn.decomposition import PCA

import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsRegressor
from sklearn.metrics import mean_absolute_error

def loadData():
    df_train = pd.read_csv("Data/Train_data.csv")
    df_train = df_train.rename(columns={'Unnamed: 0': 'Id'})
    
    #Objetivo
    df_train_labels = df_train["Made Donation in March 2007"]
    
    #Columnas no neceserias
    #Id, solo identifica, el resultado va indexado en paralelo, por lo que el id no es necesario
    #Made Donation in March 2007 Objetivo
    df_train_values = df_train.drop(columns=['Id','Made Donation in March 2007'])

    df_test_values = pd.read_csv("Data/Test_data.csv")
    df_test_values = df_test_values.rename(columns={'Unnamed: 0': 'Id'})
    df_test_values = df_test_values.drop(columns=['Id'])

    return df_train_values, df_train_labels, df_test_values


def initialStudy(data):
    
    #Normalización
    min_max_scaler = preprocessing.MinMaxScaler()
    states = min_max_scaler.fit_transform(data)

	#PCA Se explica el 92% de la varización de los datos
    estimator = PCA (n_components = 2)
    X_pca = estimator.fit_transform(states)
    print(estimator.explained_variance_ratio_, "Varianza de los datos explicada: ",np.sum(estimator.explained_variance_ratio_), "%")

    #Visualización inicial de los datos
    
    x = []
    y = []

    for i in X_pca:
        x.append(i[0])
        y.append(i[1])
    
    plt.scatter(x, y, alpha=0.5)
    plt.show()

def studyKValue(df_train_values, df_train_labels):
    
    X_train, X_test, Y_train, Y_test = train_test_split(df_train_values, df_train_labels,test_size=0.33, random_state=42)
    
    maes_uniform = []
    maes_distance = []

    for k in range(2,15):
        for w in ['uniform','distance']:
            neigh = KNeighborsRegressor(n_neighbors=k, weights = w)
            neigh.fit(X_train, Y_train) 
            predictions = neigh.predict(X_test)
            mae = mean_absolute_error(Y_test,predictions)
            if w == 'uniform':
                maes_uniform.append(mae)
            else:
                maes_distance.append(mae)

    #Grafica de los scores
    plt.plot(range(2,15),maes_uniform,'b',label="Uniform")
    plt.plot(range(2,15),maes_distance, 'r',label="Distance")
    plt.ylabel('Mae')
    plt.xlabel('k - value')
    plt.legend()
    plt.show()

    #El mejor valor se obtiene con K = 2 y uniform



def doKNN(df_train_values, df_train_labels, df_test_values):
    
    studyKValue(df_train_values, df_train_labels)
    
    neigh = KNeighborsRegressor(n_neighbors=2, weights = 'uniform')
    neigh.fit(df_train_values, df_train_labels) 
    predictions = neigh.predict(df_test_values)

    return predictions

def doSubmitFile(predictions):
    df = pd.read_csv("Data/BloodDonationSubmissionFormat.csv")
    df["Made Donation in March 2007"] = predictions
    df.to_csv("Data/Ready_BloodDonationSubmissionFormat.csv",index=False)

def main():
    df_train_values, df_train_labels, df_test_values = loadData()

    #initialStudy(df_train_values)

    predictions = doKNN(df_train_values, df_train_labels, df_test_values)
    doSubmitFile(predictions)
    
    

if __name__ == "__main__":
    main()